#include <stdio.h>
#include <stdlib.h>
#include "../kitgalera.h"
#define MAXPILHA 5

int main()
{
    struct pilhas{
        int itens[MAXPILHA];
        int topo,fim;
    };

    struct pilhas pilha;

    int controle;
    int selecao;
    int inicializa;

    controle = 0;
    selecao = 0;
    inicializa = 0;

    printf("\n\n\tInicializando a pilha\n");
    pilha.topo = MAXPILHA - 1;
    pilha.fim = MAXPILHA - 1;
    while(controle < MAXPILHA){
        pilha.itens[controle]='\0';
        controle++;
    }
    system("timeout 3 > pilha.log");

    while (selecao!=6){
        limpaTela();
        criaTela();
        printf("\t");
        selecao = lerInt(0);
        printf("\tValor escolhido: %i\n",selecao);
        switch (selecao){
            case 1:
                printf("\tInicializando a pilha\n");
                pilha.topo = MAXPILHA - 1;
                pilha.fim = MAXPILHA - 1;
                while(controle < MAXPILHA){
                    pilha.itens[controle]='\0';
                    controle++;
                }
                printf("\tTodos dados foram apagados\n");
                break;
            case 2:
                if ((pilha.topo <= MAXPILHA -1)&&(pilha.topo >=0)){
                    printf("\tInserindo elemento\n\t");
                    pilha.itens[pilha.topo]=lerInt(0);
                    pilha.topo--;
                }else{
                printf("\tPilha cheia\n\t");
                }
                break;
            case 3:
                printf("\n\tMostra topo/fim da pilha\n");
                printf("\tInicio da pilha: %i\n"
                       "\tFim da pilha: %i\n",pilha.topo,pilha.fim);
                break;
            case 4:
                printf("\n\tListando elementos\n");
                for (controle = pilha.topo+1; controle <= pilha.fim; controle++){
                    printf("\tposicao: %i - valor: %i\n", controle,pilha.itens[controle]);
                }

                break;
            case 5:
                if ((pilha.topo == pilha.fim)&&(pilha.itens[pilha.topo]=='\0')){
                    printf("\n\tNao ha elemento para remover\n");
                }else{
                    printf("\n\tRemovendo elemento do topo\n");
                    pilha.topo++;
                    printf("\n\tValor removido: %i\n",pilha.itens[pilha.topo]);
                    pilha.itens[pilha.topo]='\0';
                }
                break;
            case 6:
                 printf("\tSaindo\n");
                break;
            default :
                printf("\tSaindo\n");
                selecao = 6;
                break;

        }

         system("timeout 3 > pilha.log");

    }
    return 0;
}

limpaTela(){
    system("cls");
}

criaTela(){
    printf("\n\n");
    printf("\t==================================\n");
    printf("\t|        ESCOLHA UMA OPCAO       |\n");
    printf("\t==================================\n");
    printf("\t| 1 - Inicializar a pilha        |\n");
    printf("\t| 2 - Inserir elemento           |\n");
    printf("\t| 3 - Exibir Inicio / Fim        |\n");
    printf("\t| 4 - Lista elementos            |\n");
    printf("\t| 5 - Removendo elemento do topo |\n");
    printf("\t| 6 - Sair                       |\n");
    printf("\t==================================\n");
}

