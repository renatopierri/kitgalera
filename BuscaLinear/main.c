#include <stdio.h>
#include <stdlib.h>
#define MAX 20

//Busca Linear
//Array de itens a serem pesquisados

int arrayDeInt[MAX] = {1,2,3,4,6,7,9,11,12,14,15,16,17,19,33,34,43,45,55,66};

void printline(int conta){
    int controle;
        for (controle = 0; controle  < conta - 1; controle++){
            printf("=");
        }
        printf("\n");
}

//Fazendo a busca
int encontrar(int dado){
    int comparacao = 0;
    int indice = -1;
    int controle;

//Percorrendo todos os itens
    for (controle = 0; controle < MAX; controle++){
        //Contando o nr de comparações
        comparacao++;
        //Se encontrar o dado, faz-se a quebra do loop
        if(dado == arrayDeInt[controle]){
            indice = controle;
            break;
        }
    }
    printf("Total de comparacoes efetuadas: %d\n", comparacao);
    return indice;
}

void display(){
    int controle;
    printf("[");
    //Navegando por todos os itens
    for (controle = 0; controle < MAX;controle++){
        printf("%d ", arrayDeInt[controle]);
    }
    printf("]\n");
}

int main()
{
    int localizacao;
    printf("\nArray de entrada: ");
    display();
    printline(50);
    //Encontrando um valor
    localizacao = encontrar(14);

    //Se o elemento foi encontrado
    if (localizacao != -1){
        printf("Elemento encontrado na posicao %d\n", (localizacao +1));
    } else {
        printf("Elemento nao encontrado\n");
    }
    return 0;
}
