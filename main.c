#include <stdio.h>
#include <stdlib.h>
#include "kitgalera.h"

/* KitGalera � um conjunto de fun��es b�sicas para acelerar
 * o estudo de C da galera. Serve para n�o  ficar  perdendo
 * tempo fazendo valida��o de dados, quando o importante  �
 * aprender estrutura de  dados, por exemplo.
 * Se voc� nem sabe fazer uma  entrada  de  dados  simples,
 * estude esse c�digo que ele tamb�m pode ser �til �  voc�.
 * Melhorias e corre��es s�o bem vindas.
 * Professor Renato.
 */

//Se VERB for = 1 � modo verboso. Imprime mais na tela.
const VERB = 0;

int main()
{

    char letra;
    int inteiro;
    float numeroFloat;
    double numeroDouble;
    char palavra[20];

    int tam_palavra;

    tam_palavra = sizeof(palavra);
    lerPalavra(palavra,tam_palavra);
    printf("a palavra digitada eh: %s\n",palavra);


    letra = lerLetra(VERB);
    printf("Letra digitada = %c\n", letra);

    inteiro = lerInt(VERB);
    printf("Inteiro digitado = %i\n", inteiro);

    numeroFloat = lerFloat(VERB);
    printf("Nr Float: %f\n", numeroFloat);

    numeroDouble = lerDouble(VERB);
    printf("Nr Double: %lf\n", numeroDouble);

    return 0;
}

