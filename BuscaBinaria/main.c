#include <stdio.h>
#include <stdlib.h>
#define MAX 20

//Busca Binaria
//Array de itens a serem pesquisados

int arrayDeInt[MAX] = {1,2,3,4,6,7,9,11,12,14,15,16,17,19,33,34,43,45,55,66};

void printline(int conta){
    int controle;
        for (controle = 0; controle  < conta - 1; controle++){
            printf("=");
        }
        printf("\n");
}

int encontrar(int dado){
    int limiteInferior = 0;
    int limiteSuperior = MAX - 1;
    int pontoCentral = -1;
    int comparacao = 0;
    int indice = -1;

    while(limiteInferior <= limiteSuperior){
        printf("Comparacao %d\n", (comparacao + 1));
        printf("limiteInferior: %d\nlimiteSuperior: %d\n", limiteInferior, limiteSuperior);
        comparacao++;

        //computando o ponto central

        pontoCentral = (limiteInferior + limiteSuperior)/2;

        //Dado encontrado:

        if(arrayDeInt[pontoCentral] == dado){
            indice = pontoCentral;
            break;
        }else{
            //Caso o dado seja maior
            //O dado est� na metade superior
            if (arrayDeInt[pontoCentral] < dado){
                limiteInferior = pontoCentral + 1;
            }else{
                //Dado � menor
                //Dado est� na metade inferior
                limiteSuperior = pontoCentral - 1;
            }
        }

    }
    printf("Total de comparacoes efetuadas: %d\n", comparacao);
    return indice;
    }
void display(){
    int controle;
    printf("[");
    //Navegando por todos os itens
    for (controle = 0; controle < MAX;controle++){
        printf("%d ", arrayDeInt[controle]);
    }
    printf("]\n");
}

int main(){

    int localizacao;
    printf("\nArray de entrada: ");
    display();
    printline(50);
    //Encontrando um valor
    localizacao = encontrar(7);

    //Se o elemento foi encontrado
    if (localizacao != -1){
        printf("Elemento encontrado na posicao %d\n", (localizacao));
    } else {
        printf("Elemento nao encontrado\n");
    }
    return 0;
}
