#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "kitgalera.h"


/* KitGalera � um conjunto de fun��es b�sicas para acelerar
 * o estudo de C da galera. Serve para n�o  ficar  perdendo
 * tempo fazendo valida��o de dados, quando o importante  �
 * aprender estrutura de  dados, por exemplo.
 * Se voc� nem sabe fazer uma  entrada  de  dados  simples,
 * estude esse c�digo que ele tamb�m pode ser �til �  voc�.
 * Melhorias e corre��es s�o bem vindas.
 * Professor Renato.
 */

void lerPalavra(char* palavra, int tamanhoPalavra){

    int controle = 0;
    char parametro[] = {'\0','\0','\0','\0','\0','\0'};
    char tmp;

    printf("Digite uma palavra de ate %i caractere(s):\n",tamanhoPalavra);

    //Montando o par�metro do scanf
    sprintf(parametro, "%%%ds", tamanhoPalavra);

    do{
        //Lendo a entrada
        //Comando equivalente: scanf("%10s", palavra);
        scanf(parametro,palavra);

        //Esvaziando o buffer caso existam outros caracteres
        while ((tmp = getchar()) != '\n' && tmp != EOF);

        //Verificando se pelo menos um caractere foi digitado
        if(strlen(palavra) >= 1) {
            controle = 1;
        }
    }while (controle != 1);

return;
}

char lerLetra(int msg){
    char letra;
    char tmp;

    printf("Digite um caractere:\n");

    //Pegando o primeiro caractere do buffer
    letra = getchar();

    //Esvaziando o buffer caso existam outros caracteres
    while ((tmp = getchar()) != '\n' && tmp != EOF);

    if (msg == 1){
        printf("Caractere digitado: ");
        putchar(letra);
        printf("\n");
    }

    return letra;
}

int lerInt(int msg){
    char *end;
    char linha[100];
    int nrInt;

    printf("Digite um nr int:\n");

    do {

         if (!fgets(linha, sizeof linha, stdin))
            break;
         // remove \n
         linha[strlen(linha) - 1] = 0;
         nrInt = strtol(linha, &end, 10);

    } while (end != linha + strlen(linha));

    if(msg ==1){
        printf("Nr int digitado: %i\n", nrInt);
    }

    return nrInt;
}

float lerFloat(int msg){

    double nrFloat;
    char linha[100];
    int convertido=0;
    int conta;

    nrFloat = 4.0;

    printf("Digite um nr float:\n");

    do{
        //Lendo o float como string
        scanf("%100s", linha);

        //Verificando se contem caracteres v�lidos
        conta = contaCaracteres( linha, '.' );
        if((strspn(linha, "0123456789.") == strlen(linha))&& (conta <=1)) {

            //Convertendo para float
            nrFloat = strtof(linha, NULL);
            convertido = 1;
        }
    }while (convertido != 1);

    //Exibindo o nr float
    if(msg ==1){
        printf("Nr float digitado: %f\n", nrFloat);
    }

    return nrFloat;
}

double lerDouble(int msg){

    double nrDouble;
    char linha[100];
    int convertido=0;
    int conta;

    nrDouble = 4.0;

    printf("Digite um nr double:\n");

    do{
        //Lendo o double como string
        scanf("%100s", linha);

        //Verificando se contem caracteres v�lidos
        conta = contaCaracteres( linha, '.' );
        if((strspn(linha, "0123456789.") == strlen(linha))&& (conta <=1)) {

            //Convertendo para double
            nrDouble = strtod(linha, NULL);
            convertido = 1;
        }
    }while (convertido != 1);

    //Exibindo o nr double
    if(msg ==1){
        printf("Nr double digitado: %lf\n", nrDouble);
    }

    return nrDouble;

}


//Autor desta fun��o: http://stackoverflow.com/users/12950/tvanfosson
int contaCaracteres( char* s, char c ){
    return *s == '\0'
           ? 0
           : contaCaracteres( s + 1, c ) + (*s == c);
}
