#include <stdio.h>
#include <stdbool.h>
#define MAX 7

/*
* Implementa��o recursiva do algoritmo Quick Sort
*/

//Declarando um array global. N�o gosto de vari�veis globais.
int intArray[MAX] = {4,6,3,2,1,9,7};
//int intArray[MAX] = {7,6,5,4,3,2,1};

int main(){
    printf("\n\tArray inicial: ");
    display();
    imprimeLinha(51);
    quickSort(0,MAX-1);
    printf("\tArray final: ");
    display();
    imprimeLinha(51);
}

// Imprimindo um separador na tela para ficar bonito(perfumaria)
    void imprimeLinha(int conta){
        int controle;
        for(controle = 0; controle < conta-1; controle++){
            printf("=");
        }
        printf("=\n\n");
    }
// Fim da perfumaria

// Imprimindo todos elementos do array, entre colchetes
    void display(){
        int controle;
        printf("[ ");
        for(controle = 0; controle < MAX; controle++){
            printf("%d ",intArray[controle]);
        }
        printf("]\n\n");
    }
// Fim da impress�o dos elementos do array

// Recebe dois subscritos e troca entre si dois elementos de um array.
void troca(int subscrito1, int subscrito2){
    int temp = intArray[subscrito1];
    intArray[subscrito1] = intArray[subscrito2];
    intArray[subscrito2] = temp;
}
// Fim da troca dos elementos do array

// Varrendo o vetor pelas duas pontas
int particao(int esquerda, int direita, int pivo){
    int pontaEsquerda = esquerda -1;
    int pontaDireita = direita;
    while(true){
        while(intArray[++pontaEsquerda] < pivo){}  //nao faz nada

        while(pontaDireita > 0 &&
              intArray[--pontaDireita] > pivo){} //nao faz nada

        if(pontaEsquerda >=
           pontaDireita){
            break;
        }
        else{
            printf("\tTroca array[%d] pelo array[%d]: %d,%d\n", pontaEsquerda,
                    pontaDireita, intArray[pontaEsquerda],intArray[pontaDireita]);
            troca(pontaEsquerda,pontaDireita);
            printf("\tArray apos atualizar: ");
            display();
        }
    }
    if(pontaEsquerda != direita){
        printf("\tTroca array[%d] pelo array[%d]: %d,%d\n", pontaEsquerda,
                direita, intArray[pontaEsquerda],intArray[direita]);
        troca(pontaEsquerda,direita);
        printf("\tArray apos atualizar: ");
        display();
    }
    return pontaEsquerda;
}
// Fim da varredura

// Fun��o quickSort � recursiva.
void quickSort(int esquerda, int direita){
    int pivo, pontoParticao;

    if(direita - esquerda <= 0){
        return;
    }
    else{
        pivo = intArray[direita];
        pontoParticao = particao(esquerda, direita, pivo);
        quickSort(esquerda,pontoParticao-1);
        quickSort(pontoParticao+1,direita);
    }
}
// Fim da fun��o quickSort


