#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "kitgalera.h"


void desafio2();
void main(){

    size_t tam_Leitura;
    size_t tam_Invertido;
    size_t nr_Letras;
    int controle = 0;

    //Crie dois arrays de char com 10 posi��es, com o nome de �leitura� e �invertido�.
    //Na declara��o, inicialize os arrays �leitura� e �invertido� com o caractere �\0� (null).

    //Inicializando como na "Vida de Programador"
    char leitura[]={'\0','\0','\0','\0','\0','\0','\0','\0','\0','\0'};

    //Aplicando boas pr�ticas de programa��o na inicializa��o.
    char invertido[10];
    for (controle = 0; controle < 10;controle ++){
        invertido[controle] = '\0';
    }

    //Comprimento do array de char
    tam_Leitura = sizeof(leitura);

    //Comprimento do texto, dentro do array de char
    nr_Letras = strlen(leitura);

    //Leia uma palavra da entrada. Se digitar <Enter> sem nada, fica aguardando uma entrada.
    //A palavra lida � inserida no array �leitura�, descartando eventuais caracteres que sobrem.
    lerPalavra(leitura,tam_Leitura);

    //O array �leitura� � impresso na tela. Caso encontre um caractere �\0�, ele n�o ser� impresso.
    printf("a palavra digitada eh: %s\n",leitura);

    //O array �invertido� recebe a invers�o do conte�do do array �leitura�.
    tam_Leitura = strlen(leitura);
    tam_Leitura--;

    for (controle = 0; controle <= tam_Leitura; controle++){
        //Aplicando a equa��o da reta aqui
        //Maiores detalhes sobre aplica��es da equa��o da reta,
        //em um projeto real, veja esse post: https://goo.gl/WPnl31
        invertido[-1*(controle - tam_Leitura)] = leitura[controle];
    }

    //Imprime o array 'invertido' e identifica se � pal�ndromo.
    if (strcmp(invertido,leitura)==0){
        printf("A palavra %s eh PALINDROMA\n" ,leitura);
    }else{
        printf("A palavra %s nao eh palindroma de %s\n", leitura, invertido);
    }
    desafio2();
    return 0;
}

void desafio2(){
    //Definindo a constante LIN para linha e COL para coluna
        const LIN = 2;
        const COL = 3;
        const AUT = 1;
    //Declarando a matriz
        char matriz[LIN][COL];
        int linha, coluna, controle;
    //Pula uma linha e inicializa 'controle'
        printf("\n");
        controle = 0;
    //Populando a matriz
        for (linha = 0; linha < LIN; linha++){
            //Percorrendo as colunas da matriz
                for (coluna = 0; coluna < COL; coluna++){
                    //Pega o valor de controle e coloca na matriz
                        if(AUT == 1){
                            matriz[linha][coluna] = 65 + controle;
                        }else{
                            matriz[linha][coluna] = lerLetra(0);
                        }
                    //Incrementa o valor de controle
                        controle ++;
                }
        }

    //Percorrendo as linhas da matriz
        for (linha = 0; linha < LIN; linha++){
            //D� um tab para desencostar da lateral esquerda da tela
                printf("\t");
            //Percorrendo as colunas da matriz
                for (coluna = 0; coluna < COL; coluna++){
                    //Imprime o valor corrente da matriz e d� um tab
                        printf("%c\t",matriz[linha][coluna]);
                    //Incrementa o valor de controle
                        controle ++;
                }
            //Ao terminar de montar a linha, pula para a pr�xima linha
                printf("\n");
        }
}
