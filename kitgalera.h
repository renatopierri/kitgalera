#ifndef KITGALERA_H_INCLUDED
#define KITGALERA_H_INCLUDED

/* KitGalera � um conjunto de fun��es b�sicas para acelerar
 * o estudo de C da galera. Serve para n�o  ficar  perdendo
 * tempo fazendo valida��o de dados, quando o importante  �
 * aprender estrutura de  dados, por exemplo.
 * Se voc� nem sabe fazer uma  entrada  de  dados  simples,
 * estude esse c�digo que ele tamb�m pode ser �til �  voc�.
 * Melhorias e corre��es s�o bem vindas.
 * Professor Renato.
 */

void lerPalavra(char* palavra, int tamanhoPalavra);

char lerLetra(int msg);

int lerInt(int msg);

float lerFloat(int msg);

double lerDouble(int msg);

int contaCaracteres( char* s, char c );

#endif // KITGALERA_H_INCLUDED
